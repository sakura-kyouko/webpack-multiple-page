# webpack-multiple-page

Empty project.

## Building and running on localhost

First install dependencies:

```sh
npm install
```

To create a production build:

```sh
npm run build-prod
```

To create a development build:

```sh
npm run build-dev
```

## Running

```sh
npm run serve
```

## Credits

Made with [createapp.dev](https://createapp.dev/)

## Patch

### regenerator-runtime

在 IE8 下页面的入口文件顶部区域一开始就使用异步语法，可能会出现 polyfill 在 regeneratorRuntime 初始化之后。这里已通过补丁解决其初始化中需要的支持，如果不想通过使用补丁解决就手动避免在入口文件顶部区域出现异步语法。

### preact

preact/compat 包下使用了 Object.defineProperty 方法无法兼容 IE8 这里对源码进行了修改。在 IE8 下类组件的生命周期只能用旧的名称无法使用 UNSAFE\_ 开头的名称；JSX 属性上的 className 也无法正常使用。
