const pages = [
  {
    chunk: 'index',
    filename: 'index.html',
    script: './src/pages/index/index.tsx',
    template: 'src/pages/index/index.pug'
  }
]

module.exports.pages = pages
