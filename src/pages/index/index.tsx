if (process.env.NODE_ENV === 'development') {
  require('preact/debug')
}

import { render } from 'preact'
import './index.scss'
import styles from './index.module.scss'
import TestPug from './test.pug'
import { App } from './app'

const description = TestPug({
  titleClass: styles.title,
  desc: '基于 webpack 5 兼容 IE8 的多页面示例'
})

$('#test').html(description)

render(<App />, document.querySelector('#root')!)
