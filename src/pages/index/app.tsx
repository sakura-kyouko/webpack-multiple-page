import { useState, useCallback } from 'preact/hooks'
import { CopInput } from '@/components/CopInput'
import styles from './index.module.scss'

function Counter() {
  const [value, setValue] = useState(0)
  const increment = useCallback(() => {
    setValue(value + 1)
  }, [value])

  return (
    <div>
      Counter: {value}
      <button class={styles.counterBtn} onClick={increment}>
        Increment
      </button>
    </div>
  )
}

export function App() {
  const [values, setValues] = useState({ nickname: '' })

  return (
    <div class={styles.form}>
      <Counter />
      <div class={styles.formItem}>
        <label class={styles.formLabel}>姓名</label>
        <div class={styles.inputBlock}>
          <CopInput
            class={styles.input}
            type="text"
            name="nickname"
            value={values.nickname}
            placeholder="请输入"
            onInput={(value) => {
              setValues({ nickname: value })
            }}
          />
        </div>
      </div>
      <div class={styles.formItem}>
        <div class={styles.inputBlock}>
          <button
            onClick={() => {
              console.log('nickname: ', values.nickname)
            }}
          >
            提交
          </button>
        </div>
      </div>
    </div>
  )
}
