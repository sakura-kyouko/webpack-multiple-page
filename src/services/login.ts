import { setToken } from '@/utils/login'
import { get, postData } from '@/utils/request'

export async function simpleAuth(username: string, password: string) {
  const token = await postData('auth/token', { username, password })
  setToken(token)
}

export function getUserInfo() {
  return get<{ userId: number; nickname: string }>('auth/userInfo')
}
