const prefixUrl = process.env.SERVICE_URL!

export function getFullUrl(url: string) {
  if (!/^(\/|https?)/.test(url)) {
    return prefixUrl + '/' + url
  }
  return url
}
