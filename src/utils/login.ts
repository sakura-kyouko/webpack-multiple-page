import * as Cookies from 'js-cookie'

export function getToken() {
  return Cookies.get('token')
}

export function setToken(val: string) {
  return Cookies.set('token', val)
}

export function removeToken() {
  Cookies.remove('token')
}
