declare module '*.css'
declare module '*.scss'
declare module '*.png'
declare module '*.jpg'
declare module '*.webp'

declare module '*.pug' {
  export default function template(locals?: Record<any, any>): string
}
